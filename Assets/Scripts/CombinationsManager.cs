﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum EnvironmentObjectType 
{ 
	Wood, 
	Rock,
	Fire,
	Straw,
	Spark,
	Pollution,
	Charcoal,
	Ember,
	Metal,
	RoundRock,
	BigRock,
	Water,
	Stick,
	Pickaxe,
	Petrol,
	Gear
};

[System.Serializable]
public struct TypePair
{
	public EnvironmentObjectType type1;
	public EnvironmentObjectType type2;

	public TypePair(EnvironmentObjectType type1, EnvironmentObjectType type2)
	{
		this.type1 = type1;
		this.type2 = type2;
	}
}

[System.Serializable]
public struct CombinationPair
{
	public TypePair pair;
	public GameObject[] result;
}

public class CombinationsManager : Singleton<CombinationsManager>
{
	public GameObject pollutionGameObject;

	public List<CombinationPair> combinationPairs = new List<CombinationPair>();

	public static List<CombinationPair> foundCombinations = new List<CombinationPair>();
	private static Dictionary<TypePair, GameObject[]> combinationsDictionary = new Dictionary<TypePair, GameObject[]>();

	private void Awake()
	{
		combinationsDictionary.Clear();
		for (int i = 0; i < combinationPairs.Count; i++)
		{
			combinationsDictionary.Add(combinationPairs[i].pair, combinationPairs[i].result);
		}
	}

	public static GameObject[] GetCombinationResult(EnvironmentObjectType type1, EnvironmentObjectType type2)
	{
		if (combinationsDictionary.ContainsKey(new TypePair(type1, type2)))
		{
			return combinationsDictionary[new TypePair(type1, type2)];
		}
		else if (combinationsDictionary.ContainsKey(new TypePair(type2, type1)))
		{
			return combinationsDictionary[new TypePair(type2, type1)];
		}
		else
		{
			return new GameObject[0];
		}
	}

	public static void RegisterCombinedObjects(EnvironmentObjectType type1, EnvironmentObjectType type2)
	{
		for (int i = 0; i < instance.combinationPairs.Count; i++)
		{
			CombinationPair combinationPair = instance.combinationPairs[i];
			if (combinationPair.pair.type1 == type1 && combinationPair.pair.type2 == type2 ||
				combinationPair.pair.type1 == type2 && combinationPair.pair.type2 == type1)
			{
				RegisterFoundCombination(combinationPair);
				return;
			}
		}
	}

	[ContextMenu("Register all combinations")]
	private void RegisterAllCombinations()
	{
		for (int i = 0; i < combinationPairs.Count; i++)
		{
			RegisterFoundCombination(combinationPairs[i]);
		}
	}

	private static void RegisterFoundCombination(CombinationPair foundCombination)
	{
		if (!foundCombinations.Contains(foundCombination))
			foundCombinations.Add(foundCombination);
	}
}

public class ObjectsNames
{
	private static Dictionary<EnvironmentObjectType, string> objectNames = new Dictionary<EnvironmentObjectType, string>
	{
		{ EnvironmentObjectType.Charcoal, "Carvão" },
		{ EnvironmentObjectType.Ember, "Brasa" },
		{ EnvironmentObjectType.Fire, "Fogo" },
		{ EnvironmentObjectType.Pollution, "Poluição" },
		{ EnvironmentObjectType.Rock, "Pedra" },
		{ EnvironmentObjectType.Spark, "Faísca" },
		{ EnvironmentObjectType.Straw, "Palha" },
		{ EnvironmentObjectType.Wood, "Madeira" },
		{ EnvironmentObjectType.Metal, "Metal" },
		{ EnvironmentObjectType.RoundRock, "Roda" },
		{ EnvironmentObjectType.BigRock, "Pedra gigante" },
		{ EnvironmentObjectType.Water, "Água" },
		{ EnvironmentObjectType.Stick, "Bastão" },
		{ EnvironmentObjectType.Pickaxe, "Picareta" },
		{ EnvironmentObjectType.Petrol, "Petróleo" },
		{ EnvironmentObjectType.Gear, "Engrenagem" }
	};

	public static string GetName(EnvironmentObjectType type)
	{
		return objectNames[type];
	}
}