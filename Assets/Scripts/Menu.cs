﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using DG.Tweening;

public class Menu : MonoBehaviour 
{
	private void Start()
	{
		ScreenFader.FadeIn();
	}

	public void QuitGame()
	{
		Application.Quit();
	}

	public void StartGame()
	{
		ScreenFader.OnFadeOutFinished += LoadMainScene;
		ScreenFader.FadeOut();
	}

	private void LoadMainScene()
	{
		ScreenFader.OnFadeOutFinished -= LoadMainScene;
		SceneManager.LoadScene("MainScene");
	}
}
