﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public static class ExtensionMethods
{
	public static Transform FindChildRecursive(this Transform transform, string childName)
	{
		Transform result = transform.Find(childName);
		if (result)
			return result;

		for(int i = 0; i < transform.childCount; i++)
		{
			result = transform.GetChild(i).FindChildRecursive(childName);
			if (result)
				return result;
		}

		return null;
	}

	public static void SetAlpha(this Image image, float alpha)
	{
		Color newColor = image.color;
		newColor.a = alpha;
		image.color = newColor;
	}
}
