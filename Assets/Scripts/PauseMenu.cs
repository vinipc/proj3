﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using DG.Tweening;

public class PauseMenu : Singleton<PauseMenu> 
{
	public static bool isPaused = false;
	private GameObject pauseParent;

	private void Awake()
	{
		pauseParent = transform.GetChild(0).gameObject;
	}

	public void OpenPauseMenu()
	{
		isPaused = true;
		pauseParent.SetActive(true);

		Time.timeScale = 0f;
		Cursor.lockState = CursorLockMode.None;
		Cursor.visible = true;
	}

	public void ClosePauseMenu()
	{
		isPaused = false;
		pauseParent.SetActive(false);

		Time.timeScale = 1f;
		Cursor.lockState = CursorLockMode.Locked;
		Cursor.visible = false;
	}

	public void MenuButtonPressed()
	{
		ScreenFader.OnFadeOutFinished += LoadMenu;
		ScreenFader.FadeOut().SetUpdate(true);
	}

	private void LoadMenu()
	{
		ScreenFader.OnFadeOutFinished -= LoadMenu;
		Time.timeScale = 1f;
		Cursor.lockState = CursorLockMode.None;
		Cursor.visible = true;
		SceneManager.LoadScene("Menu");
	}
}