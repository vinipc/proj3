﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;

public class EnvironmentObject : MonoBehaviour 
{
	public const float REGROWTH_POWER = 2f;

	public GameObject combinationFailParticles;

	public EnvironmentObjectType type;
	public bool fixedObject;
	public bool isShrinking = false;

	public float spawnForce = 250f;
	public float deformation = 0.1f;

	new private Rigidbody rigidbody;

	private void Awake()
	{
		transform.SetParent(GameObject.Find("_EnvironmentObjects").transform);
		rigidbody = GetComponent<Rigidbody>();
		gameObject.name = ObjectsNames.GetName(type);

		if (!fixedObject)
		{
			float lowerDeformation = 1f - deformation;
			float upperDeformation = 1f + deformation;
			transform.Rotate(Vector3.up * Random.Range(0f, 360f));
			transform.localScale = new Vector3(Random.Range(lowerDeformation, upperDeformation), Random.Range(lowerDeformation, upperDeformation), Random.Range(lowerDeformation, upperDeformation));
		}
	}

	private void Update()
	{
		if (!isShrinking && transform.localScale.x < 1f)
		{
			float objectScale = transform.localScale.x;
			objectScale = Mathf.Min(objectScale + REGROWTH_POWER * Time.deltaTime, 1f);
			transform.localScale = Vector3.one * objectScale;
		}
	}

	private void OnCollisionEnter(Collision coll)
	{
		EnvironmentObject otherEnvironmentObject = coll.gameObject.GetComponent<EnvironmentObject>();	
		float velocitySqrMagnitude = rigidbody.velocity.sqrMagnitude;
		float otherCollVelocitySqrMagnitude;
		GameObject[] combinationResult;

		if (otherEnvironmentObject)
		{
			otherCollVelocitySqrMagnitude = otherEnvironmentObject.GetComponent<Rigidbody>().velocity.sqrMagnitude;
			combinationResult = CombinationsManager.GetCombinationResult(type, otherEnvironmentObject.type);
			Vector3 collisionPoint = coll.contacts[0].point;

			if (combinationResult.Length > 0 && velocitySqrMagnitude > otherCollVelocitySqrMagnitude)
			{
				CombineWithObject(otherEnvironmentObject, collisionPoint);
			}
			else if (combinationResult == null && velocitySqrMagnitude > otherCollVelocitySqrMagnitude)
			{
				Instantiate(combinationFailParticles, collisionPoint, combinationFailParticles.transform.rotation);
			}
		}
	}

	private void CombineWithObject(EnvironmentObject otherObject, Vector3 collisionPoint)
	{
		GameObject[] combinationResults = CombinationsManager.GetCombinationResult(type, otherObject.type);
		Vector3 position = collisionPoint + Random.insideUnitSphere;

		for (int i = 0; i < combinationResults.Length; i++)
		{
			GameObject newObject = Instantiate(combinationResults[i]);
			Rigidbody newRigidbody = newObject.GetComponent<Rigidbody>();
			
			newObject.transform.position = position;
			newObject.transform.DOScale(Vector3.zero, 1f).From();
			newRigidbody.AddTorque(new Vector3(Random.Range(-50f, 50f), Random.Range(-50f, 50f), Random.Range(-50f, 50f)));
			newRigidbody.AddForce(new Vector3(Random.value * spawnForce, Random.value * spawnForce, 0f));
		}

		GetComponent<Collider>().enabled = false;
		otherObject.GetComponent<Collider>().enabled = false;

		Destroy(gameObject);
		Destroy(otherObject.gameObject);

		for (int i = 0; i < 2; i++)
		{
			Rigidbody pollutionRigidbody = ((GameObject) Instantiate(CombinationsManager.instance.pollutionGameObject, collisionPoint + Random.insideUnitSphere * 0.5f, Quaternion.identity)).GetComponent<Rigidbody>();
			pollutionRigidbody.AddForce(new Vector3(Random.value * spawnForce, Random.value * spawnForce, 0f));
			pollutionRigidbody.transform.DOScale(Vector3.zero, 1f).From();
		}

		CombinationsManager.RegisterCombinedObjects(type, otherObject.type);
	}
}