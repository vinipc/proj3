﻿using UnityEngine;
using System.Collections;

public class WaterDarkener : MonoBehaviour 
{
	public Color targetColor;
	public int maxPollution = 50;

	private int currentLevel = 0;
	private Color originalColor;
	private MeshRenderer meshRenderer;

	private void Awake()
	{
		meshRenderer = GetComponent<MeshRenderer>();
		originalColor = meshRenderer.material.GetColor("_horizonColor");
	}

	private void OnTriggerEnter(Collider coll)
	{
		EnvironmentObject environmentObject = coll.GetComponent<EnvironmentObject>();
		if (environmentObject && environmentObject.type == EnvironmentObjectType.Pollution)
		{
			currentLevel++;
			Color newColor = Color.Lerp(originalColor, targetColor, (float) currentLevel / (float) maxPollution);
			meshRenderer.material.SetColor("_horizonColor", newColor);

			if (currentLevel == maxPollution)
				Destroy(this);
		}
	}
}
