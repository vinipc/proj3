﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class GameMaster : Singleton<GameMaster> 
{
	private float resetTimer = 0f;
	private MouseLook[] playerMouseLook;
	private FirstPersonDrifter playerMovement;

	private void Awake()
	{
		playerMouseLook = FindObjectsOfType<MouseLook>();
		playerMovement = FindObjectOfType<FirstPersonDrifter>();
		DisablePlayerMovement();
	}

	private void Start()
	{
		ScreenFader.OnFadeInFinished += OnFadeInFinished;
		ScreenFader.FadeIn(1f);
	}

	private void Update()
	{
		if (Input.GetKey(KeyCode.R))
		{
			resetTimer += Time.deltaTime;
			if (resetTimer >= 1f)
			{
				SceneManager.LoadScene(SceneManager.GetActiveScene().name);
			}
		}
		else
		{
			resetTimer = 0f;
		}

		if (Input.GetKeyDown(KeyCode.Escape))
		{
			if (PauseMenu.isPaused)
				PauseMenu.instance.ClosePauseMenu();
			else
				PauseMenu.instance.OpenPauseMenu();
		}
	}

	private void OnFadeInFinished()
	{
		ScreenFader.OnFadeInFinished -= OnFadeInFinished;
		EnablePlayerMovement();
	}

	private void EnablePlayerMovement()
	{
		playerMovement.enabled = true;
		for (int i = 0; i < playerMouseLook.Length; i++)
		{
			playerMouseLook[i].enabled = true;
		}
	}

	private void DisablePlayerMovement()
	{
		playerMovement.enabled = false;
		for (int i = 0; i < playerMouseLook.Length; i++)
		{
			playerMouseLook[i].enabled = false;
		}
	}
}
