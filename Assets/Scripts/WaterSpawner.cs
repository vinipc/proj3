﻿using UnityEngine;
using System.Collections;

public class WaterSpawner : MonoBehaviour 
{
	public GameObject waterPrefab;
	public int maxNumberOfWater;
	private BoxCollider boxCollider;

	private float cooldown;

	private void Awake()
	{
		boxCollider = GetComponent<BoxCollider>();
	}

	private void Update()
	{
		cooldown -= Time.deltaTime;
		if (cooldown <= 0f)
		{
			int numberOfWatersInside = 0;
			WaterEnvironmentObject[] allWaters = FindObjectsOfType<WaterEnvironmentObject>();
			
			for (int i = 0; i < allWaters.Length; i++)
			{
				if (boxCollider.bounds.Contains(allWaters[i].transform.position))
					numberOfWatersInside++;
			}
			
			if (numberOfWatersInside < maxNumberOfWater)
			{
				Transform instantiatedWater = Instantiate<GameObject>(waterPrefab).transform;
				instantiatedWater.GetComponentInChildren<Renderer>().enabled = false;
				Vector3 position = transform.position;
				position.x += Random.Range(-1f, 1f);
				position.z += Random.Range(-1f, 1f);
				instantiatedWater.position = position;
				instantiatedWater.localScale = Vector3.zero;
			}

			cooldown = 0.5f;
		}

	}
}