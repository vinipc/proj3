﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class CabineController : MonoBehaviour 
{
	public Transform[] pathWaypoints;

	private Vector3 lastPosition;

	[ContextMenu("Follow path")]
	public void FollowPath()
	{
		Debug.Log("follow path");
		Vector3[] pathPoints = new Vector3[pathWaypoints.Length];
		for (int i = 0; i < pathWaypoints.Length; i++)
		{
			pathPoints[i] = pathWaypoints[i].position;
		}

		FirstPersonDrifter player = FindObjectOfType<FirstPersonDrifter>();
		player.enabled = false;
		lastPosition = transform.position;

		transform.DOPath(pathPoints, 10f, PathType.CatmullRom)
			.OnUpdate(() => UpdatePlayerPosition(player))
			.OnComplete(() => UnlockPlayer(player));
	}

	private void UpdatePlayerPosition(FirstPersonDrifter player)
	{
		Vector3 deltaPosition = transform.position - lastPosition;
		player.transform.position += deltaPosition;
		lastPosition = transform.position;
	}

	private void UnlockPlayer(FirstPersonDrifter player)
	{
		player.enabled = true;
		player.transform.SetParent(null);
	}
}
