﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class FoundCombinationsLister : MonoBehaviour 
{
	public GameObject textPrefab;

	private void OnEnable()
	{
		ShowList();
	}

	private void OnDisable()
	{
		ClearList();
	}

	private void ShowList()
	{
		List<CombinationPair> combinationPairs = CombinationsManager.instance.combinationPairs;

		for (int i = 0; i < combinationPairs.Count; i++)
		{
			Text newText = ((GameObject) Instantiate(textPrefab, transform)).GetComponent<Text>();
			newText.transform.localScale = Vector3.one;

			string leftObjectName, rightObjectName, resultObjectName;
			resultObjectName = ObjectsNames.GetName(combinationPairs[i].result[0].GetComponent<EnvironmentObject>().type);

			if (CombinationsManager.foundCombinations.Contains(combinationPairs[i]))
			{
				leftObjectName = ObjectsNames.GetName(combinationPairs[i].pair.type1);
				rightObjectName = ObjectsNames.GetName(combinationPairs[i].pair.type2);
			}
			else
			{
				leftObjectName = rightObjectName = "?";
			}

			newText.text = string.Concat(leftObjectName, " + ", rightObjectName, " = ", resultObjectName);
		}
	}

	private void ClearList()
	{
		for (int i = 0; i < transform.childCount; i++)
		{
			Destroy(transform.GetChild(i).gameObject);
		}
	}
}