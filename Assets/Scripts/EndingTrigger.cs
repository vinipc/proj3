﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using DG.Tweening;

public class EndingTrigger : MonoBehaviour 
{
	private void OnTriggerEnter(Collider collider)
	{
		if (collider.CompareTag("Player"))
		{
			EndGame();
		}
	}

	private void EndGame()
	{
		ScreenFader.OnFadeOutFinished += LoadMenu;
		ScreenFader.FadeOut().SetDelay(1f);
	}

	private void LoadMenu()
	{
		ScreenFader.OnFadeOutFinished -= LoadMenu;

		Cursor.lockState = CursorLockMode.None;
		Cursor.visible = true;

		SceneManager.LoadScene("Menu");
	}
}