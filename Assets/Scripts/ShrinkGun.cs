﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class ShrinkGun : MonoBehaviour 
{
	public LayerMask layerMask;
	public float rayDistance = 2f;
	public float shrinkPower = 0.3f;
	public float minimumScale = 0.15f;
	public float shootForce = 3000f;

	private Transform storageRoot;
	private Transform gunPoint;
	private ParticleSystem suckingParticles;
	private ParticleSystem shootingParticles;

	public GameObject waterObject;

	[ReadOnly]
	public Transform storedObject;
	[ReadOnly]
	public EnvironmentObject currentTarget;
	[ReadOnly]
	public Transform currentPointedObect;

	private void Awake()
	{
		storageRoot = transform.FindChildRecursive("_storage");
		gunPoint = transform.FindChildRecursive("_gunpoint");
		suckingParticles = transform.FindChildRecursive("_suckingParticles").GetComponent<ParticleSystem>();
		shootingParticles = transform.FindChildRecursive("_shootingParticles").GetComponent<ParticleSystem>();
	}

	private void Update()
	{
		DetectTarget();

		if (PauseMenu.isPaused)
			return;

		if (Input.GetButtonDown("Fire") && storedObject != null)
		{
			ShootObject();
		}
		else if (Input.GetButtonDown("Fire") && storedObject == null)
		{
			ShootRay();
		}

		if (Input.GetButton("Fire"))
		{
			suckingParticles.Play();
			if (currentTarget != null)
			{
				ShrinkTarget();
			}
		}

		if (Input.GetButtonUp("Fire"))
		{
			suckingParticles.Stop();
			if (currentTarget != null)
				currentTarget.isShrinking = false;
			currentTarget = null;
		}
	}

	private void ShootRay()
	{
		if (currentTarget != null) // storedObject == null
		{
			ShrinkTarget();
			return;
		} 
		else if (currentTarget == null) // storedObjecet == null && currentTarget == null
		{
			RaycastHit rayHit = new RaycastHit();
			if (Physics.Raycast(transform.position, transform.forward, out rayHit, rayDistance, layerMask))
			{
				EnvironmentObject environmentObject = rayHit.transform.GetComponent<EnvironmentObject>();
				if (!environmentObject.fixedObject)
				{
					currentTarget = rayHit.transform.GetComponent<EnvironmentObject>();
				}
			}	
		}
	}

	private void ShrinkTarget()
	{
		float objectScale = Mathf.Max(new float[3]{ currentTarget.transform.localScale.x, currentTarget.transform.localScale.y, currentTarget.transform.localScale.y });

		if (objectScale <= minimumScale)
		{
			StoreTarget();
		}
		else
		{
			objectScale = Mathf.Max(objectScale - shrinkPower * Time.deltaTime, minimumScale);
			currentTarget.transform.localScale = Vector3.one * objectScale;
			currentTarget.isShrinking = true;
		}
	}

	private void StoreTarget()
	{
		storedObject = currentTarget.transform;
		currentTarget = null;

		storedObject.position = storageRoot.position;
		storedObject.SetParent(storageRoot);
		storedObject.GetComponent<Rigidbody>().isKinematic = true;
		storedObject.GetComponentInChildren<Renderer>().enabled = true;
		storedObject.localScale = Vector3.one * minimumScale * 0.75f;
		storedObject.localRotation = storageRoot.localRotation;
	}

	private void ShootObject()
	{
		storedObject.GetComponent<EnvironmentObject>().isShrinking = false;

		shootingParticles.Clear();
		shootingParticles.Play();

		storedObject.SetParent(null);
		storedObject.position = gunPoint.position;
		storedObject.GetComponent<Rigidbody>().isKinematic = false;
		storedObject.GetComponent<Rigidbody>().AddForce(transform.forward * shootForce);
		storedObject.DOScale(Vector3.one, 0.1f);

		storedObject = null;
	}

	private void DetectTarget()
	{
		RaycastHit rayHit = new RaycastHit();
		if (Physics.Raycast(transform.position, transform.forward, out rayHit, rayDistance, layerMask))
		{
			currentPointedObect = rayHit.transform;
		}
		else
		{
			currentPointedObect = null;
		}
	}
}