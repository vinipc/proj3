﻿using UnityEngine;
using System.Collections;

public class Gizmo : MonoBehaviour 
{
	public Color color = Color.red;
	public float radius = 5f;

	public void OnDrawGizmos()
	{
		Gizmos.color = color;
		Gizmos.DrawSphere(transform.position, radius);
	}
}