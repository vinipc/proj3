﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using DG.Tweening;

public class ScreenFader : Singleton<ScreenFader> 
{
	public const float FADE_TIME = 0.5f;

	public static event Action OnFadeInFinished, OnFadeOutFinished;

	private static Image blackscreen;

	private void Awake()
	{
		blackscreen = instance.GetComponent<Image>();
		gameObject.SetActive(false);
	}

	public static Tween FadeIn(float duration = FADE_TIME)
	{
		instance.gameObject.SetActive(true);
		blackscreen.SetAlpha(1f);
		return blackscreen.DOFade(0f, duration).OnComplete(FinishFadeIn);
	}

	public static Tween FadeOut(float duration = FADE_TIME)
	{
		instance.gameObject.SetActive(true);
		return blackscreen.DOFade(1f, duration).OnComplete(FinishFadeOut);
	}

	private static void FinishFadeIn()
	{
		if (OnFadeInFinished != null)
			OnFadeInFinished();

		instance.gameObject.SetActive(false);
	}

	private static void FinishFadeOut()
	{
		if (OnFadeOutFinished != null)
			OnFadeOutFinished();
	}
}