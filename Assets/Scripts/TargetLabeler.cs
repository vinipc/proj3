﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TargetLabeler : MonoBehaviour 
{
	private ShrinkGun shrinkGun;
	private Text label;

	private void Awake()
	{
		shrinkGun = FindObjectOfType<ShrinkGun>();
		label = GetComponent<Text>();
	}

	private void Update()
	{
		label.text = 
			shrinkGun.currentPointedObect ? shrinkGun.currentPointedObect.name :
			shrinkGun.currentTarget ? shrinkGun.currentTarget.name :
			"";
	}
}
