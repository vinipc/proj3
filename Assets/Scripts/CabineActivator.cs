﻿using UnityEngine;
using System.Collections;

public class CabineActivator : MonoBehaviour 
{
	public CabineController cabineController;

	public void OnTriggerEnter(Collider coll)
	{
		if (coll.gameObject.CompareTag("Player"))
		{
			cabineController.FollowPath();
			Destroy(gameObject);
		}
	}
}
