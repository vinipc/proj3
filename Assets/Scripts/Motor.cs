﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class Motor : MonoBehaviour 
{
	public Collider cabinBarrier;

	private bool addedSparks, addedPetrol, addedGear;
	private Vector3 originalScale;

	public float heightStretchDuration = 0.15f;
	public float unstrecthDuration = 0.1f;

	private void Awake()
	{
		originalScale = transform.localScale;
	}

	private void OnCollisionEnter(Collision coll)
	{
		EnvironmentObject environmentObject = coll.gameObject.GetComponent<EnvironmentObject>();
		if (environmentObject != null)
		{
			EnvironmentObjectType type = environmentObject.type;
			if (type == EnvironmentObjectType.Spark && !addedSparks)
			{
				addedSparks = true;
				Destroy(environmentObject.gameObject);
				UpdateState();
			}
			else if (type == EnvironmentObjectType.Petrol && !addedPetrol)
			{
				addedPetrol = true;
				Destroy(environmentObject.gameObject);
				UpdateState();
			}
			else if (type == EnvironmentObjectType.Gear && !addedGear)
			{
				addedGear = true;
				Destroy(environmentObject.gameObject);
				UpdateState();
			}
		}
	}

	private void UpdateState()
	{
		if (addedSparks && addedPetrol && addedGear)
		{
			Destroy(cabinBarrier.gameObject);
			StartShake();
		}
	}

	[ContextMenu("Start shake")]
	private void StartShake()
	{
		Sequence sequence = DOTween.Sequence();
		sequence.Append(StretchHeight());
		sequence.Append(Unstretch());
		
		sequence.SetLoops(-1, LoopType.Restart);
	}

	private Tween StretchHeight()
	{
		Sequence sequence = DOTween.Sequence();
		sequence.Append(transform.DOScaleY(originalScale.y * 1.1f, heightStretchDuration));
		sequence.Join(transform.DOScaleZ(originalScale.z * 0.9f, heightStretchDuration));

		return sequence;
	}

	private Tween Unstretch()
	{
		Sequence sequence = DOTween.Sequence();
		sequence.Append(transform.DOScaleY(originalScale.y, unstrecthDuration));
		sequence.Join(transform.DOScaleZ(originalScale.z, unstrecthDuration));

		return sequence;
	}
}